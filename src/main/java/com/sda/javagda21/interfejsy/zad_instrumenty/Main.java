package com.sda.javagda21.interfejsy.zad_instrumenty;

public class Main {
    public static void main(String[] args) {
        Bęben bęben = new Bęben();
        Instrumentalny obiekt = new Bęben();

        obiekt.graj();
        bęben.graj();

        if(obiekt instanceof Bęben){
            System.out.println("TAK!");
        }
    }
}
