package com.sda.javagda21.interfejsy.zad_czlonkowie_rodziny;

public class Corka implements ICzlonekRodziny {
    private String imie;

    public Corka(String imie) {
        this.imie = imie;
    }

    public void przedstawSie() {
        System.out.println("jestem córeczką" + imie);
    }

    @Override
    public boolean czyDorosly() {
        return false;
    }
}
