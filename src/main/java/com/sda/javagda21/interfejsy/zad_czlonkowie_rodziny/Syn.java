package com.sda.javagda21.interfejsy.zad_czlonkowie_rodziny;

public class Syn implements ICzlonekRodziny {
    private String imie;

    public Syn(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("Kto pyta? - " + imie);
    }

    @Override
    public boolean czyDorosly() {
        return false;
    }
}
