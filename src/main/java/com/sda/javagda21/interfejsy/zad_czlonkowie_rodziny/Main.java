package com.sda.javagda21.interfejsy.zad_czlonkowie_rodziny;

public class Main {
    public static void main(String[] args) {
        ICzlonekRodziny[] tablica = new ICzlonekRodziny[5];
        tablica[0] = new Matka("Ziuta");
        tablica[1] = new Ojciec("Gienek");
        tablica[2] = new Syn("Marian");
        tablica[3] = new Corka("Gabrycha");
        tablica[4] = new Ciocia();

        for (ICzlonekRodziny iCzlonekRodziny : tablica) {
            iCzlonekRodziny.przedstawSie();
            System.out.println(iCzlonekRodziny.czyDorosly());
        }

    }
}
