package com.sda.javagda21.interfejsy.zad_czlonkowie_rodziny;

public class Matka implements ICzlonekRodziny {
    private String imie;

    public Matka(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("Jestem mamusia." + imie);
    }

    @Override
    public boolean czyDorosly() {
        return true;
    }
}
