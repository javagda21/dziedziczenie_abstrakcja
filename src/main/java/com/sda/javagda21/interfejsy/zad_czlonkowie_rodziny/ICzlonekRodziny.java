package com.sda.javagda21.interfejsy.zad_czlonkowie_rodziny;

public interface ICzlonekRodziny {
    public default void przedstawSie(){
        System.out.println("Jestem sobie czlonkiem rodziny.");
    }

    public boolean czyDorosly();
}
