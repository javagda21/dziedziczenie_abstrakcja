package com.sda.javagda21.interfejsy.zad_czlonkowie_rodziny;

public class Ojciec implements ICzlonekRodziny {
    private String imie;

    public Ojciec(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("I am Your father." + imie);
    }

    @Override
    public boolean czyDorosly() {
        return true;
    }
}
