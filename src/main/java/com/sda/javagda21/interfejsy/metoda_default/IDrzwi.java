package com.sda.javagda21.interfejsy.metoda_default;

public interface IDrzwi {
    public default void zamknij(){
        System.out.println("Drzwi zamykaja sie");
    }
}
