package com.sda.javagda21.interfejsy.metoda_default;

public class DrzwiSamochodowe implements IDrzwi, Zamykalny {

    @Override
    public void zamknij() {
        IDrzwi.super.zamknij(); // <-- wywołanie metody defaultowej z IDrzwi
        Zamykalny.super.zamknij(); // <-- wywołanie metody defaultowej z Zamykalny
        System.out.println("Nowa implementacja"); // własna impelentacja

    }
}
