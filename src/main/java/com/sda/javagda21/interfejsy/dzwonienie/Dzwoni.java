package com.sda.javagda21.interfejsy.dzwonienie;

public interface Dzwoni {
    // domyślnie WSZYSTKIE POLA W INTERFEJSIE SĄ PSF (public static final)
    public static final String NUMER_ALARMOWY = "112";

    void zadzwon(String numer);
    void zadzwonNaAlarmowy();
}
