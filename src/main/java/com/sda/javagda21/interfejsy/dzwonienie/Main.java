package com.sda.javagda21.interfejsy.dzwonienie;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Telefon telefon = new Telefon();
        Scanner scanner = new Scanner(System.in);

        String numer;
        do {
            numer = scanner.next();

            if (numer.equalsIgnoreCase("alarm")) {
                telefon.zadzwonNaAlarmowy();
            } else {
                telefon.zadzwon(numer);
            }
        } while (!numer.equalsIgnoreCase("quit"));
    }
}
