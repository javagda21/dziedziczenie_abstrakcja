package com.sda.javagda21.interfejsy.dzwonienie;

import java.util.Random;

public class Telefon implements Dzwoni {
    public void zadzwon(String numer) {
        if (new Random().nextBoolean()) { // niech random wylosuje mi czy dodzwonie sie czy nie
            System.out.println("Dzwonie na numer: " + numer);
        } else {
            System.out.println("Nie można się dodzwonić.");
        }
    }

    public void zadzwonNaAlarmowy() {
        zadzwon(NUMER_ALARMOWY);
    }
}
