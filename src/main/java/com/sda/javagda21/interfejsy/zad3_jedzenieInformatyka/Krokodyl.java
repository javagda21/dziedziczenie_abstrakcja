package com.sda.javagda21.interfejsy.zad3_jedzenieInformatyka;

public class Krokodyl implements Jedzacy {
    private int ilePosilkowZjadl;
    private int ileGramowZjadl;

    public int ilePosilkowZjedzone() {
        return ilePosilkowZjadl;
    }

    public int ileGramowZjedzone() {
        return ileGramowZjadl;
    }

    public void jedz(Pokarm pokarm) {
        ilePosilkowZjadl++;
        ileGramowZjadl += pokarm.getWaga();
    }
}
