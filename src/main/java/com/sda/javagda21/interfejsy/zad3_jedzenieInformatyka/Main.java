package com.sda.javagda21.interfejsy.zad3_jedzenieInformatyka;

public class Main {
    public static void main(String[] args) {
        Pokarm[] pokarms = new Pokarm[7];
        pokarms[0] = new Pokarm("Jabłka", TypPokarmu.OWOCE, 500);
        pokarms[1] = new Pokarm("Ser pleśniowy", TypPokarmu.NABIAL, 2000);
        pokarms[2] = new Pokarm("Stek", TypPokarmu.MIESO, 5000);
        pokarms[3] = new Pokarm("Pierś z kurczaka", TypPokarmu.MIESO, 10000);
        pokarms[4] = new Pokarm("Ręka", TypPokarmu.MIESO, 3200);
        pokarms[5] = new Pokarm("Żubr", TypPokarmu.MIESO, 113200);
        pokarms[6] = new Pokarm("Żubr", TypPokarmu.PIWO, 3200);

        Jedzacy[] jedzacy = new Jedzacy[3];
        jedzacy[0] = new Wegetarianin();
        jedzacy[1] = new Krokodyl();
        jedzacy[2] = new Programista();



        for (Pokarm pokarm : pokarms) {

            // każdy musi zjeść każdy posiłek
            for (Jedzacy jedzacy1 : jedzacy) {
                jedzacy1.jedz(pokarm);
            }

            // po każdym posiłku każdy chwali się ile zjadł
            for (Jedzacy jedzacy1 : jedzacy) {
                System.out.println(jedzacy1 + " zjadł " + jedzacy1.ilePosilkowZjedzone() + " o wadze : " + jedzacy1.ileGramowZjedzone());
            }
            System.out.println();
        }
    }
}
