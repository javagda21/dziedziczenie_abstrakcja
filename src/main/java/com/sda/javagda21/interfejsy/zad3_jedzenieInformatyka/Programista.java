package com.sda.javagda21.interfejsy.zad3_jedzenieInformatyka;

public class Programista implements Jedzacy {
    private int ilePosilkowZjadl;
    private int ileGramowZjadl;

    public int ilePosilkowZjedzone() {
        return ilePosilkowZjadl;
    }

    public int ileGramowZjedzone() {
        return ileGramowZjadl;
    }

    public void jedz(Pokarm pokarm) {
        if (pokarm.getTypPokarmu() != TypPokarmu.NABIAL) {
            ilePosilkowZjadl++;
            ileGramowZjadl += pokarm.getWaga();
        }
    }
}
