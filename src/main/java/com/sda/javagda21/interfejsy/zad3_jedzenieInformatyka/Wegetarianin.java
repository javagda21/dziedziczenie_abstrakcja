package com.sda.javagda21.interfejsy.zad3_jedzenieInformatyka;

public class Wegetarianin implements Jedzacy {
    private int ilePosilkowZjadl;
    private int ileGramowZjadl;

    public void jedz(Pokarm pokarm) {
        if (pokarm.getTypPokarmu() != TypPokarmu.MIESO
                || pokarm.getTypPokarmu() == TypPokarmu.PIWO) {
            ilePosilkowZjadl++;
            ileGramowZjadl += pokarm.getWaga();
        }
    }

    public int ilePosilkowZjedzone() {
        return ilePosilkowZjadl;
    }

    public int ileGramowZjedzone() {
        return ileGramowZjadl;
    }
}
