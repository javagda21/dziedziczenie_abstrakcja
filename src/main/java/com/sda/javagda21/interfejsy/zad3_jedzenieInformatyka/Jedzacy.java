package com.sda.javagda21.interfejsy.zad3_jedzenieInformatyka;

public interface Jedzacy {
     void jedz(Pokarm pokarm);

    int ilePosilkowZjedzone();

    int ileGramowZjedzone();
}
