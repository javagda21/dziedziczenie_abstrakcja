package com.sda.javagda21.obiektowosc.zad_symulator_farby;

import com.sda.javagda21.obiektowosc.zad_symulator_farby.figury.Figura;

public class SymulatorFarby {
    public static int obliczZapotrzebowanieNaFarbe(Figura[] figury, double pojemnosc) {

        double suma = 0.0;
        for (Figura figura : figury) {
            suma += figura.obliczPowierzchnie();
        }

        return (int) Math.ceil(suma / pojemnosc);
    }
}
