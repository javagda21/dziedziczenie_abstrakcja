package com.sda.javagda21.obiektowosc.zad_symulator_farby.figury;

public class Trojkat extends Figura {
    private double dlugoscH;

    public Trojkat(double dlugoscA, double dlugoscH) {
        super(dlugoscA);
        this.dlugoscH = dlugoscH;
    }

    public double obliczPowierzchnie() {
        return (dlugoscH * dlugoscA) / 2.0;
    }
}
