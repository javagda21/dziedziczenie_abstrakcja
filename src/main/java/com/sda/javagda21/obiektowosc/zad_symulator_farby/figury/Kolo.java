package com.sda.javagda21.obiektowosc.zad_symulator_farby.figury;

public class Kolo extends Figura {
    public Kolo(double dlugoscA) {
        super(dlugoscA);
    }

    public double obliczPowierzchnie() {
        return dlugoscA * dlugoscA * Math.PI;
    }
}
