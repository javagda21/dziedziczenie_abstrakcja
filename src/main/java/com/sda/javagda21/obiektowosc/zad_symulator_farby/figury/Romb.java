package com.sda.javagda21.obiektowosc.zad_symulator_farby.figury;

public class Romb extends Figura {
    private double dlugoscB;

    // zakładam że to przekątne
    public Romb(double dlugoscA, double dlugoscB) {
        super(dlugoscA);
        this.dlugoscB = dlugoscB;
    }

    public double obliczPowierzchnie() {
        return (dlugoscA * dlugoscB) / 2.0;
    }
}
