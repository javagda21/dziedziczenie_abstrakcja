package com.sda.javagda21.obiektowosc.zad_symulator_farby;

import com.sda.javagda21.obiektowosc.zad_symulator_farby.figury.*;

public class Main {
    public static void main(String[] args) {
        Figura[] figury = new Figura[5];
        figury[0] = new Kwadrat(5);
        figury[1] = new Prostokat(5, 10);
        figury[2] = new Romb(10, 10);
        figury[3] = new Trojkat(3, 4);
        figury[4] = new Kolo(11);

        for (Figura figura : figury) {
            System.out.println(figura); // brakuje nadpisania metody toString
        }

        // wypisanie na ekran zapotrzebowania na farbe (w ilościach pojemników pojemności 50)
        System.out.println(SymulatorFarby.obliczZapotrzebowanieNaFarbe(figury, 50));

    }

    // todo: 1. Koło
    // todo: 2. Prostokąt
    // todo: 3. Romb
    // todo: 4. Trójkąt

    // Math.PI <--

    // todo: 5. Stwórz po jednej instancji każdej klasy w main i sprawdź czy metody
    //  działają poprawnie
}
