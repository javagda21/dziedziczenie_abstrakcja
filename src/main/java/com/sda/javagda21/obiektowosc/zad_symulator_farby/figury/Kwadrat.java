package com.sda.javagda21.obiektowosc.zad_symulator_farby.figury;

public class Kwadrat extends Figura {

    public Kwadrat(double dlugoscA) {
        super(dlugoscA);
    }

    public double obliczPowierzchnie() {
        return dlugoscA * dlugoscA;
    }

    @Override
    public String toString() {
        return "Kwadrat{" +
                "dlugoscA=" + dlugoscA +
                '}';
    }
}
