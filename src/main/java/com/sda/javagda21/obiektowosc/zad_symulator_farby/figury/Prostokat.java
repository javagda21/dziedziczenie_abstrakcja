package com.sda.javagda21.obiektowosc.zad_symulator_farby.figury;

public class Prostokat extends Figura {
    private double dlugoscB;

    public Prostokat(double dlugoscA, double dlugoscB) {
        super(dlugoscA);
        this.dlugoscB = dlugoscB;
    }

    public double obliczPowierzchnie() {
        return dlugoscA * dlugoscB;
    }
}
