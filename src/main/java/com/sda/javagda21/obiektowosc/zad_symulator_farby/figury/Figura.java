package com.sda.javagda21.obiektowosc.zad_symulator_farby.figury;

public abstract class Figura {
    protected double dlugoscA;

    public Figura(double dlugoscA) {
        this.dlugoscA = dlugoscA;
    }

    public abstract double obliczPowierzchnie();

    @Override
    public String toString() {
        return "Figura{" +
                "dlugoscA=" + dlugoscA +
                '}';
    }
}
