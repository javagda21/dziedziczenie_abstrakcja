package com.sda.javagda21.obiektowosc.zad_kompozycja;

public class Biblioteka {
    private Egzemplarz[] egzemplarze;

    public Egzemplarz[] szukajPoTytule(String tytul) {
        int ileJestEgzemplarzyZtakimTytulem = 0;

        for (Egzemplarz egzemplarz : egzemplarze) {
            if (egzemplarz.tytul.equalsIgnoreCase(tytul)) {
                ileJestEgzemplarzyZtakimTytulem++;
            }
        }

        Egzemplarz[] egzemplarzeZSzukanymTytulem = new Egzemplarz[ileJestEgzemplarzyZtakimTytulem];
        int licznikWstawiania = 0;
        for (int i = 0; i < egzemplarze.length; i++) {
            if (egzemplarze[i].tytul.equalsIgnoreCase(tytul)) {
                egzemplarzeZSzukanymTytulem[licznikWstawiania] = egzemplarze[i];

                licznikWstawiania++;
            }
        }

        return egzemplarzeZSzukanymTytulem;
    }

    public Egzemplarz[] szukajPoAutorze(String autor) {
        int ileJestEgzemplarzyZtakimTytulem = 0;

        for (Egzemplarz egzemplarz : egzemplarze) {
            if (egzemplarz.autor.imie.contains(autor) || egzemplarz.autor.nazwisko.contains(autor)) {
                ileJestEgzemplarzyZtakimTytulem++;
            }
        }

        Egzemplarz[] egzemplarzeZSzukanymTytulem = new Egzemplarz[ileJestEgzemplarzyZtakimTytulem];
        int licznikWstawiania = 0;
        for (int i = 0; i < egzemplarze.length; i++) {
            if (egzemplarze[i].autor.imie.contains(autor) || egzemplarze[i].autor.nazwisko.contains(autor)) {
                egzemplarzeZSzukanymTytulem[licznikWstawiania] = egzemplarze[i];
                licznikWstawiania++;
            }
        }

        return egzemplarzeZSzukanymTytulem;
    }

    public Egzemplarz[] szukajPoAutorzeITytule(String fraza) {
        Egzemplarz[] poAutorze = szukajPoAutorze(fraza);
        Egzemplarz[] poTytule = szukajPoTytule(fraza);

        Egzemplarz[] sumaWynikow = new Egzemplarz[poAutorze.length + poTytule.length];
        for (int i = 0; i < poAutorze.length; i++) {
            sumaWynikow[i] = poAutorze[i];
        }

        for (int i = 0; i < poTytule.length; i++) {
            // przesuamy indeks o długość tablicy poAutorze
            sumaWynikow[i + poAutorze.length] = poTytule[i];
        }

        return sumaWynikow;
    }
}
