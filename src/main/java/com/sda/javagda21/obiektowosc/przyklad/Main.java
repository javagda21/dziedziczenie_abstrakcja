package com.sda.javagda21.obiektowosc.przyklad;

public class Main {
    public static void main(String[] args) {
        Ptak[] ptaki = new Ptak[3];
        ptaki[0] = new Kukulka();
        ptaki[1] = new Bocian();
        ptaki[2] = new Kukulka();

//        Kukulka[] kukulkas = new Kukulka[2];
//        kukulkas[0] = new Kukulka();
//        kukulkas[1] = new Bocian();
//
//        Bocian[] bocians = new Bocian[2];
//        bocians[0] = new Bocian();
//        bocians[1] = new Kukulka();


        // Polimorfizm klas:
        Ptak sobiePtaszek = new Kukulka();
        sobiePtaszek.metoda(); // Kukuleczka


        // Polimorfizm metod
        Kukulka instancja = new Kukulka();
        instancja.metoda();
        instancja.metoda(10);
    }
}
