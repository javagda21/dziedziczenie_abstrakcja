package com.sda.javagda21.obiektowosc.przyklad;

public abstract class Ptak {
    protected String nazwa;

    // metody abstrakcyjne nie mają ciała
    public abstract void spiewaj();

    protected void wypiszSwojeImie(){
        System.out.println(nazwa);
    }

    public void metoda(){
        System.out.println("Ptaszek");
    }

}
