package com.sda.javagda21.obiektowosc.przyklad;

public class Kukulka extends Ptak {
    public void spiewaj() {
        System.out.println(nazwa);
        System.out.println("Ku ku");
    }

    @Override
    public void metoda() {
        System.out.println("Kulkuleczka");
    }

    public void metoda(int ileRazy) {
        for (int i = 0; i < ileRazy; i++) {
            metoda();
        }
    }
}
